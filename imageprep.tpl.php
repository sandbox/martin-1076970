<?php
// $Id$

/**
 * @file
 *
 * Display PI prep page.
 */
?>
<?php if (user_is_anonymous()): ?>
<p>If you are a club member <a href="/user">log in</a> prior to using this service, it will help you.</p>
<?php endif; ?>
<p>
    This service will prepare your images for entry into the club projected image competitions. It will resize them
    to a maximum of <?php print imageprep_width;?>x<?php print imageprep_height;?> pixels, set the author, set the title,
    set the resolution to 72 pixels per inch and convert the image to jpeg format. You can upload jpeg or
    tiff images for conversion.
</p>
<p>
    This service will <em>not</em> change the colour space of an image. If there
    is any existing author or title information it knows it might be there but is unable to read it
    so it leaves the author and title alone.
</p>
<p><em>Only click the submit button once and wait patiently, it will take a long time to upload your image and
    convert it.</em></p>