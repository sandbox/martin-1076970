<?php
// $Id$

/**
 * @file
 *
 * Display PI prep page.
 */
?>
<p>Your image has been changed as follows:</p>
<table>
    <tr><td>Width</td><td><?php print check_plain($x);?></td></tr>
    <tr><td>Height</td><td><?php print check_plain($y);?></td></tr>
    <tr><td>Format</td><td>jpeg</td></tr>
    <tr><td>Resolution</td><td>72 DPI</td></tr>
<?php if ($hasiptc != 'y'): ?>
    <tr><td>Author</td><td><?php print check_plain($author);?></td></tr>
    <tr><td>Title</td><td><?php print check_plain($title);?></td></tr>
<?php endif; ?>
</table>

<noscript>
<p><a href="/imageprepdownload">Download</a></p>
</noscript>
<script language="JavaScript">
  document.write('<p>Your image should download automatically, if it fails to do so <a href="/imageprepdownload">click here</a>.</p>');
</script>
<p><a href="/imageprep">Process another image</a></p>